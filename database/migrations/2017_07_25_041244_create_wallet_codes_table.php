<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_codes', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('wallet_id')->unsigned()->nullable();
			$table->foreign('wallet_id')->references('id')->on('wallets');
			$table->string('code');
			$table->integer('value');
			$table->dateTimeTz('used_at')->nullable();	
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_codes');
    }
}
