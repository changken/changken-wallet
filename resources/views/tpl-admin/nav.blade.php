		<ul class="nav nav-tabs justify-content-center">
			<li class="nav-item"><a href="{{ route('index') }}" class="nav-link">首頁</a></li>
			<li class="nav-item"><a href="{{ route('admin') }}" class="nav-link">管理中心</a></li>
			<li class="nav-item"><a href="{{ route('admin.addCode') }}" class="nav-link">新增錢包代碼</a></li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					用戶管理 <span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a href="{{ route('user.index') }}" class="dropdown-item">用戶列表</a>
				</div>
			</li>
		</ul>