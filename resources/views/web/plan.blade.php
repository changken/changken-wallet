@extends('tpl.main')

@section('title', '會員方案')

@section('content')
		<table class="table table-bordered">
			<tr>
				<td>方案</td>
				<td>一般</td>
				<td>商店</td>
			</tr>
			<tr>
				<td>儲值+購買儲值卡</td>
				<td><span class="badge badge-success">O</span></td>
				<td><span class="badge badge-success">O</span></td>
			</tr>
			<tr>
				<td>支付</td>
				<td><span class="badge badge-success">O</span></td>
				<td><span class="badge badge-success">O</span></td>
			</tr>
			<tr>
				<td>Api收款(*)</td>
				<td><span class="badge badge-danger">X</span></td>
				<td><span class="badge badge-success">O</span></td>
			</tr>
			<tr>
				<td>Api金鑰數量(**)</td>
				<td><span class="badge badge-danger">N/A</span></td>
				<td><span class="badge badge-success">1個</span>***</td>
			</tr>
			<tr>
				<td>Api串接諮詢</td>
				<td><span class="badge badge-warning">O</span>****</td>
				<td><span class="badge badge-success">O</span></td>
			</tr>
			<tr>
				<td>客服</td>
				<td><span class="badge badge-success">O</span></td>
				<td><span class="badge badge-success">O</span></td>
			</tr>
			<tr>
				<td>價格</td>
				<td>免費</td>
				<td>每筆Api收款的5%(沒收款則免費)</td>
			</tr>
		</table>
		<ol>
			<li>* 2017.8.20~2017.8.31 成功註冊並申請Api的不在此限內！</li>
			<li>承上，一般會員於活動時間外使用Api收款功能所產生的費用也是每筆5%。</li>
			<li>**不包含作廢的Api金鑰，請您放心！</li>
			<li>***如需使用超過1個以上的金鑰，請您聯繫客服！</li>
			<li>****一般會員的Api串接諮詢是有限的(限2017.8.31之前註冊並申請api的使用者，只能透過email並且回覆時間>=24h)！</li>
		</ol>
@endsection