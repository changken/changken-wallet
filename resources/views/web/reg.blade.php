@extends('tpl.main')

@section('title', '註冊')

@section('content')
		@if(count($errors) > 0)
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
							<p class="text-center">{{ $error }}</p>
						@endforeach
					</div>
				</div>
			</div>
		@endif
		<form action="{{ route('regc')}}" method="post" role="form">
			@csrf
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="form-group">
						<label for="name">帳號:</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="username">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="form-group">
						<label for="email">電子郵件:</label>
						<input type="text" name="email" id="email" class="form-control" placeholder="email">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="form-group">
						<label for="pw">密碼:</label>
						<input type="password" name="pw" id="pw" class="form-control" placeholder="password">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="form-group">
						<label for="pw2">確認密碼:</label>
						<input type="password" name="pw2" id="pw2" class="form-control" placeholder="password(again)">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="agree">我「已詳閱」並「完全同意」本站的服務條款！
						</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<button type="submit" class="btn btn-primary">註冊</button>
				</div>
			</div>
		</form>
@endsection