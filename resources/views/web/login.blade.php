@extends('tpl.main')

@section('title', '登入')

@section('content')
		<div class="row">
			<div class="col-xs-4 col-md-4 mx-auto">
				<div class="alert alert-info">
					<p class="text-center">沒有帳號？<a href="{{ route('reg') }}">馬上註冊！</a></p>
				</div>
			</div>
		</div>
		<form action="{{ route('loginc') }}" method="post" role="form">
			@csrf
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="form-group">
						<label for="name">帳號:</label>
						<input type="text" name="name" id="name" class="form-control" placeholder="username">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="form-group">
						<label for="pw">密碼:</label>
						<input type="password" name="pw" id="pw" class="form-control" placeholder="password">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="remember">記住我
						</label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-md-4 mx-auto">
					<button type="submit" class="btn btn-primary">登入</button>
				</div>
			</div>
		</form>
@endsection