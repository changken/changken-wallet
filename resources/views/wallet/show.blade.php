@extends('tpl.main')

@section('title', '我的錢包')

@section('content')
			@if($user->Wallet->value < 0)
				<div class="alert alert-danger">
					<h1 class="text-center">由於您的餘額低於0元，如再不儲值的話會被處以停權！<br/>
					<a href="{{ route('wallet.redeem') }}">立刻兌換一組錢包代碼</a></h1>
				</div>
			@endif
			<h1>您的錢包:</h1>
			<table class="table table-bordered">
				<tr>
					<td>您的大名:</td>
					<td>{{ $user->name }}</td>
				</tr>
				<tr>
					<td>錢包地址:</td>
					<td>{{ $user->Wallet->address }}</td>
				</tr>
				<tr>
					<td>錢包餘額:</td>
					@if($user->Wallet->value >= 0)
						<td class="success"><span style="font-size:36px;color:green;">NT$ {{ $user->Wallet->value }}</span></td>
					@elseif($user->Wallet->value < 0)
						<td class="danger"><span style="font-size:36px;color:red;">NT$ {{ $user->Wallet->value }}</span></td>
					@else
						<td class="info">NT$ {{ $user->Wallet->value }}</td>
					@endif
				</tr>
				<tr>
					<td>註備:</td>
					<td>changken 錢包可以讓您於本站儲值並且消費！<br/>
					每一個會員都能擁有一個錢包！<br/>
					changken 錢包擁有預先儲值、交易迅速、安全等好處！<br/>
					因應政府政策，本錢包移除轉帳功能！<br/>
					</td>
				</tr>
			</table>
@endsection