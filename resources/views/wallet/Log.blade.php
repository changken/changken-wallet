@extends('tpl.main')

@section('title', '錢包紀錄')

@section('content')
		<div class="alert alert-info">
			<p class="text-center">
				<b>提示:</b>這裡可以看到您的錢包紀錄！<br>
				如果紀錄有問題的話，請立即向客服更正~~
			</p>
		</div>
		<table class="table table-bordered">
			<tr>
				<td>編號</td>
				<td>項目</td>
				<td>金額</td>
				<td>餘額</td>
				<td>註備</td>
				<td>紀錄日期</td>
			</tr>
			@foreach($WalletLogs as $WalletLog)
				<tr>
					<td>{{ $WalletLog->id }}</td>
					<td>{{ $WalletLog->item }}</td>
					<td>NT$ {{ $WalletLog->price }}</td>
					<td>NT$ {{ $WalletLog->value }}</td>
					@if($WalletLog->note == "")
						<td>無註備</td>
					@else
						<td>{{ $WalletLog->note }}</td>
					@endif
					<td>{{ $WalletLog->created_at }}</td>
				</tr>
			@endforeach
		</table>
		{{ $WalletLogs->links() }}
@endsection