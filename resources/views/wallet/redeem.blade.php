@extends('tpl.main')

@section('title', '儲值')

@section('head')
	<script>
	$(function () {
		$('#submit').click( function () {
			if($('#key').val() == "")
			{
				alert( '要輸入錢包代碼！');
			}
			else
			{
				$.ajax({
					url:'redeem',
					data:{'key':$('#key').val()},
					type:'POST',
					dataType:'text',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					success: function(msg){
						$('#show').append(msg);
						$('#show').hide();
						//$('#show').fadeIn('slow');
						$('#show').fadeIn('fast');
						//$('#show').fadeIn(10000);
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert('發生錯誤！');
					},
					async : true
				});
			}
		});
	});
	</script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
			{{--@if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach($errors->all() as $error)
						<p class="text-center">{{ $error }}</p>
					@endforeach
				</div>
			@endif--}}
		<div class="row">
			<div class="col-xs-4 col-md-4 mx-auto">
				<div id="show"></div><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-md-4 mx-auto">
				<div class="form-group">
					<label for="key">錢包代碼:</label>
					<input type="text" name="key" id="key" size="11" class="form-control" placeholder="XXX-XXX-XXX" aria-describedby="helpBlock">
					<span id="helpBlock" class="help-block">
					你會收到類似像這樣子的序號:K1E-J48-99K，直接輸入它即可！</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-md-4 mx-auto">
				<button type="submit" id="submit" class="btn btn-primary">兌換</button>
			</div>
		</div>
@endsection