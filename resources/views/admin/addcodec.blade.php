@extends('tpl-admin.main')

@section('title', '管理中心 - 新增錢包代碼')

@section('content')
		<table class="table table-bordered">
			<tr>
				<td>錢包代碼</td>
				<td>{{ $key }}</td>
			</tr>
			<tr>
				<td>價值</td>
				<td>{{ $value }}</td>
			</tr>
		</table>
@endsection