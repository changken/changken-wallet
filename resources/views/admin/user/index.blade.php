@extends('tpl-admin.main')

@section('title', '管理中心 - 用戶列表')

@section('head')
	<script>
		function check()
		{
			return confirm('您確定要移除該帳號嗎？(此動作不可回逆！)');
		}
	</script>
@endsection

@section('content')
		<table class="table table-bordered">
			<tr>
				<td>編號</td>
				<td>帳號</td>
				<td>電郵</td>
				<td>等級</td>
				<td>狀態</td>
				<td>註冊於</td>
				<td>編輯</td>
				<td>刪除</td>
			</tr>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					@if($user->level == 'root')
						<td><span class="badge badge-success">Root</span></td>
					@elseif($user->level == 'admin')
						<td><span class="badge badge-success">管理員</span></td>
					@elseif($user->level == 'store')
						<td><span class="badge badge-success">商店會員</span></td>
					@elseif($user->level == 'user')
						<td><span class="badge badge-success">一般會員</span></td>
					@else
						<td><span class="badge badge-warning">有問題</span></td>
					@endif
					@if($user->status == 1)
						<td><span class="badge badge-success">有效</span></td>
					@else
						<td><span class="badge badge-danger">無效</span></td>
					@endif
					<td>{{ $user->created_at }}</td>
					@if($user->level == 'root')
						<td colspan="2"><span class="badge badge-info">不能更動</span></td>
					@else
						<td>
							<a href="{{ route('user.edit', [ 'user' => $user->id]) }}" class="btn btn-warning">編輯</a>
						</td>
						<td>
							<form action="{{ route('user.destroy', ['user' => $user->id ]) }}" method="post" class="form-inline" role="form" onsubmit="return check()">
								@method('DELETE')
								@csrf
								<button type="submit" class="btn btn-danger">移除</button>
							</form>
						</td>
					@endif
				</tr>
			@endforeach
		</table>
		{{ $users->links() }}
@endsection