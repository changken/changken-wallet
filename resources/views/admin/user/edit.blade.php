@extends('tpl-admin.main')

@section('title', '管理中心 - 編輯用戶')

@section('content')
			<form action="{{ route('user.update', [ 'user' => $user->id] ) }}" method="post" role="form">
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="id">使用者ID:</label>
							<input type="text" name="id" id="id" class="form-control" aria-describedby="helpBlock" value="{{ $user->id }}" readonly>
							<span class="help-block" id="helpBlock">e.g. 1 => ken(管理員)</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="name">用戶名:</label>
							<input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="email">電郵:</label>
							<input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="level">等級:</label>
							<select class="form-control" name="level" id="level">
								@if($user->level == 'admin')
									<option value="admin" selected>管理員(目前狀態)</option>
									<option value="store">商店會員</option>
									<option value="user">一般會員</option>
								@elseif($user->level == 'store')
									<option value="admin">管理員</option>
									<option value="store" selected>商店會員(目前狀態)</option>
									<option value="user">一般會員</option>
								@else
									<option value="admin">管理員</option>
									<option value="store">商店會員</option>
									<option value="user" selected>一般會員(目前狀態)</option>
								@endif
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="status">狀態:</label>
							<select class="form-control" name="status" id="status">
								@if($user->status == 1)
									<option value="1" selected>有效(目前狀態)</option>
									<option value="0">無效</option>
								@else
									<option value="1">有效</option>
									<option value="0" selected>無效(目前狀態)</option>
								@endif
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="created_at">註冊於:</label>
							<input type="text" name="created_at" id="created_at" class="form-control" value="{{ $user->created_at }}" readonly>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<button type="submit" class="btn btn-warning">編輯</button>
					</div>
				</div>
				@method('PATCH')
				@csrf
			</form>
@endsection