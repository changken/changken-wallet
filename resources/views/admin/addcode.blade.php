@extends('tpl-admin.main')

@section('title', '管理中心 - 新增錢包代碼')

@section('content')
			<form action="{{ route('admin.addCodec') }}" method="post" role="form">
				@csrf
				<div class="row">
					<div class="col-xs-4 col-md-4 mx-auto">
						<div class="form-group">
							<label for="value">價值:</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="nt">NT$</span>
								</div>
								<input type="text" name="value" id="value" class="form-control">
							</div>
						</div>
						<button type="submit" class="btn btn-primary">新增</button>
					</div>
				</div>
			</form>
@endsection