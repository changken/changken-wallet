@extends('tpl.main')

@section('title', 'changken 錢包 - 查詢完成！')

@section('content')
		<div class="alert alert-warning">
			<h2 class="text-center">
			由於查詢方沒有設定回傳網址，所以只能透過本站來顯示交易訊息！<br/>
			若對此查詢有問題者，請自行詢問查詢方！</h2>
		</div>
		<table class="table">
			<tr>
				<td>回應代碼</td>
				<td>{{ $Response->code }}</td>
			</tr>
			<tr>
				<td>回應訊息</td>
				<td>{{ $Response->message }}</td>
			</tr>
			@if($Response->code == 206)
			<tr>
				<td>訂單編號</td>
				<td>{{ $Response->data->order_id }}</td>
			</tr>
			<tr class="info">
				<td>支付人(您)</td>
				<td>{{ $Response->data->pay_from }}</td>
			</tr>
			<tr>
				<td>支付給</td>
				<td>{{ $Response->data->pay_to }}</td>
			</tr>
			<tr>
				<td>品項</td>
				<td>{{ $Response->data->item }}</td>
			</tr>
			<tr>
				<td>價格</td>
				<td>NT$ {{ $Response->data->price }}</td>
			</tr>
			<tr>
				<td>交易狀態</td>
				@if($Response->data->status == "交易成功")
					<td><span class="label-lg label label-success">交易成功</span></td>
				@else
					<td><span class="label-lg label label-danger">交易失敗</span></td>
				@endif
			</tr>
			<tr>
				<td>註備</td>
				<td>{{ $Response->data->note }}</td>
			</tr>
			<tr>
				<td>交易日期</td>
				<td>{{ $Response->data->tdate }}</td>
			</tr>
			@endif
		</table>
@endsection