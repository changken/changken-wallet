@extends('tpl.main')

@section('title', 'changken 錢包 - 交易完成！')

@section('content')
		<div class="alert alert-warning">
			<h2 class="text-center">
			由於收款方沒有設定回傳網址，所以只能透過本站來顯示交易訊息！<br/>
			若對此交易有問題者，請自行詢問收款方！</h2>
		</div>
		<table class="table">
			<tr>
				<td>回應代碼</td>
				<td>{{ $Response->code }}</td>
			</tr>
			<tr>
				<td>回應訊息</td>
				<td>{{ $Response->message }}</td>
			</tr>
			@if($Response->code == 103)
			<tr>
				<td>訂單編號</td>
				<td>{{ $Response->data->order_id }}</td>
			</tr>
			<tr>
				<td>支付人(您)</td>
				<td>{{ $Response->data->pay_from }}</td>
			</tr>
			<tr>
				<td>支付給</td>
				<td>{{ $Response->data->pay_to }}</td>
			</tr>
			@endif
		</table>
@endsection