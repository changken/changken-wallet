@extends('tpl.main')

@section('title', 'Api資訊')

@section('content')
		<div class="alert alert-warning">
			<p class="text-center">請注意！如果Api資訊不甚外洩，請第一時間通知客服！</p>
		</div>
		<table class="table table-bordered">
			<tr>
				<td>編號</td>
				<td>{{ $Api->id }}</td>
			</tr>
			<tr>
				<td>Api ID</td>
				<td>{{ $Api->Api_id }}</td>
			</tr>
			<tr>
				<td>Api金鑰</td>
				<td>{{ $Api->Api_key }}</td>
			</tr>
			<tr>
				<td>Api密碼</td>
				<td>{{ $Api->Key_pass }}</td>
			</tr>
			<tr>
				<td>Api狀態</td>
				@if($Api->Status == 'open')
					<td><span class="badge badge-success">有效</span></td>
				@elseif($Api->Status == 'close')
					<td><span class="badge badge-danger">無效</span></td>
				@endif
			</tr>
			<tr>
				<td>申請日期</td>
				<td>{{ $Api->created_at }}</td>
			</tr>
		</table>
@endsection