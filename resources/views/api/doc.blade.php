@extends('tpl.main')

@section('title', 'Api文件')

@section('content')
		<div class="alert alert-warning">
			<p class="text-center">請注意！Api最近有做更動！</p>
		</div>
		<div class="alert alert-warning">
			<p class="text-center">請注意！您必須告知支付人一旦進行支付就不可退款！</p>
		</div>
		<p class="text-center">
		changken 錢包推出的Api是一個可以讓您方便串接changken 錢包支付的一個便利方法！<br/>
		步驟一:您必須要註冊成為本站的會員，還有您必須使用商店方案！<br/>
		步驟二:點選Api專區=>申請Api<br/>
		步驟三:按下申請<br/>
		步驟四:點選Api列表即可看到您的Api金鑰！(商店方案的使用者至多可申請1個)<br/>
		步驟五:開始串接Api！您必須使用post方式到我們的api！</p>
		<h2>Api網址:http://localhost/web/pay(僅接受POST請求)</h2>
		<h2>post 欄位說明</h2>
		<table class="table">
			<tr>
				<td>欄位名稱</td>
				<td>欄位屬性(長度)</td>
				<td>註備</td>
			</tr>
			<tr>
				<td>Item</td>
				<td>string</td>
				<td>你的商品名稱</td>
			</tr>
			<tr>
				<td>Total</td>
				<td>integer</td>
				<td>你的商品總價</td>
			</tr>
			<tr>
				<td>Return_url(可選)</td>
				<td>string</td>
				<td>支付完畢時要跳轉的網址(你的商店)，沒設定就我們代為顯示！</td>
			</tr>
			<tr>
				<td>Api_id</td>
				<td>string</td>
				<td>你的Api ID</td>
			</tr>
			<tr>
				<td>Date</td>
				<td>timestamp</td>
				<td>Unix時間戳，php可使用time()產生</td>
			</tr>
			<tr>
				<td>api_token*</td>
				<td>string</td>
				<td>你的Api token</td>
			</tr>
		</table>
		<h2>Api token產生方式(php):</h2>
		<textarea rows="10" cols="80">
			$api_token_arr = [
				'Total' => 商品總價,
				'Api_id' => 錢包地址,
				'Api_key' => Api金鑰,
				'Key_pass' => Api密碼, //勿外洩(如外洩該Api金鑰只能作廢)
				'Date' => time() //Unix時間戳，php可使用time
			];
		
			ksort($api_token_arr);
		
			$api_token = hash('sha256', http_build_query($api_token_arr));
		</textarea>
		<h2>回應:(會POST到你設定的return_url)</h2>
		<table class="table">
			<tr>
				<td>欄位名稱</td>
				<td>欄位屬性(長度)</td>
				<td>註備</td>
			</tr>
			<tr>
				<td>Response</td>
				<td>json</td>
				<td>請使用php的json_decode()函數，解出來會有code和message兩組資訊！</td>
			</tr>
		</table>
		<table class="table">
			<tr>
				<td>回應代碼(code)</td>
				<td>回應資訊(message)</td>
				<td>解釋</td>
			</tr>
			<tr>
				<td>10</td>
				<td>Item 欄位不能為空！</td>
				<td>商品名稱不能為空</td>
			</tr>
			<tr>
				<td>11</td>
				<td>Total 欄位不能為空！</td>
				<td>商品總價不能為空</td>
			</tr>
			<tr>
				<td>12</td>
				<td>Api_id 欄位不能為空！</td>
				<td>Api ID 不能為空</td>
			</tr>
			<tr>
				<td>13</td>
				<td>Date 欄位不能為空！</td>
				<td>Unix時間戳不能為空</td>
			</tr>
			<tr>
				<td>14</td>
				<td>Api_token 欄位不能為空！</td>
				<td>Api token不能為空</td>
			</tr>
			<tr>
				<td>100</td>
				<td>錯誤！您沒有使用此api的權限！</td>
				<td>Api ID無效或Api狀態為未啟用</td>
			</tr>
			<tr>
				<td>111</td>
				<td>錯誤！api token驗證失敗！</td>
				<td>Api token無效</td>
			</tr>
			<tr>
				<td>101</td>
				<td>錯誤！您尚未登入changken 錢包！</td>
				<td>如回應</td>
			</tr>
			<tr>
				<td>102</td>
				<td>錯誤！您的changken 錢包餘額不足！</td>
				<td>如回應</td>
			</tr>
			<tr>
				<td>110</td>
				<td>錯誤！changken 錢包內部錯誤！請立即聯繫管理員！</td>
				<td>請聯繫管理員！(依然會扣款)</td>
			</tr>
			<tr>
				<td>103</td>
				<td>成功！您已支付此款項！</td>
				<td>如回應</td>
			</tr>
		</table>
@endsection