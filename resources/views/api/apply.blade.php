@extends('tpl.main')

@section('title', '申請Api')

@section('head')
	<script>
	$(function () {
		$('#submit').click( function () {
			$.ajax({
				url:'apply',
				data:{'key_pass':$('#key_pass').val()},
				type:'POST',
				dataType:'text',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(msg){
					$('#show').append(msg);
					$('#show').hide();
					//$('#show').fadeIn('slow');
					$('#show').fadeIn('fast');
					//$('#show').fadeIn(10000);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert('發生錯誤！');
				},
				async : true
			});
		});
	});
	</script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
		<div class="alert alert-warning">
			<p class="text-center">請注意！請您妥善保管Api資訊！<br/>
			每帳號僅限申請一組！(限會員等級為商店的會員)</p>
		</div>
		<div id="show"></div><br/>
		<p>一旦您按下了「申請」即您已詳閱並完全同意本站的「服務條款」！</p>
		@cannot('apply-api')
			<button type="submit" id="submit" class="btn btn-primary btn-lg" disabled="disabled">申請</button>
		@elsecan('apply-api')
			<button type="submit" id="submit" class="btn btn-primary btn-lg">申請</button>
		@endif
@endsection