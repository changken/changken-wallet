@extends('tpl.main')

@section('title', 'AJAX')

@section('head')
	<script>
	$(function () {
		$('#submit').click( function () {
			if($('#key').val() == "")
			{
				alert( '要輸入錢包代碼！');
			}
			else
			{
				$.ajax({
					url:'test',
					data:{"key" : $('#key').val()},
					type:'POST',
					dataType:'text',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					success: function(msg){
						var dataAll = $.parseJSON(msg);
						$('#show').append(dataAll.response);
						$('#show').hide();
						$('#show').fadeIn('slow');
						//$('#show').fadeIn('fast');
						//$('#show').fadeIn(10000);
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert('發生錯誤！');
					},
					async : true
				});
			}
		});
	});
	</script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
		<div id="show"></div><br/>
		<div class="form-group">
			<label for="key">錢包代碼:</label>
			<input type="text" name="key" id="key" size="11" class="form-control" placeholder="XXX-XXX-XXX" aria-describedby="helpBlock">
			<span id="helpBlock" class="help-block">
				你會收到類似像這樣子的序號:K1E-J48-99K，直接輸入它即可！
			</span>
		</div>
		<button type="submit" id="submit" class="btn btn-primary">兌換</button>
@endsection