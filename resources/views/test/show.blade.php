@extends('tpl.main')

@section('title', '擷取網頁')

@section('head')
	<script>
	$(function () {
		$('#submit').click( function () {
			$.ajax({
				url:'/web/member/login',
				type:'GET',
				dataType:'text',
				success: function(msg){
					//alert(msg);
					$('#show').append(msg);
					$('#show').hide();
					$('#show').fadeIn('slow');
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert('發生錯誤！');
				},
				async : true
			});
		});
	});
	</script>
@endsection

@section('content')
		<div id="show"></div><br/>
		<button type="button" id="submit" class="btn btn-primary">擷取</button>
@endsection