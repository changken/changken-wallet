@extends('tpl.main')

@section('title', '付款紀錄')

@section('content')
		<div class="alert alert-info">
			<p class="text-center">
				<b>提示:</b>這裡可以看到您的付款紀錄！
			</p>
		</div>
		@foreach($Orders as $Order)
			<table class="table table-bordered">
				<tr>
					<td>編號</td>
					<td>{{ $Order->id }}</td>
				</tr>
				<tr>
					<td>交易編號</td>
					<td>{{ $Order->order_id }}</td>
				</tr>
				<tr class="info">
					<td>支付人(您)</td>
					<td>{{ $Order->pay_from }}</td>
				</tr>
				<tr>
					<td>收款人</td>
					<td>{{ $Order->pay_to }}</td>
				</tr>
				<tr>
					<td>項目</td>
					<td>{{ $Order->item }}</td>
				</tr>
				<tr>
					<td>價格</td>
					<td>NT$ {{ $Order->price }}</td>
				</tr>
				<tr>
					<td>狀態</td>
					@if($Order->status == 1)
						<td><span class="badge badge-success">交易成功</td>
					@else
						<td><span class="badge badge-danger">交易失敗</td>
					@endif
				</tr>
				<tr>
					<td>註備</td>
					@if($Order->note == "")
						<td>無註備</td>
					@else
						<td>{{ $Order->note }}</td>
					@endif
				</tr>
				<tr>
					<td>紀錄日期</td>
					<td>{{ $Order->created_at }}</td>
				</tr>
			</table>
		@endforeach
		{{ $Orders->links() }}
@endsection
