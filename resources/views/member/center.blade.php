@extends('tpl.main')

@section('title', '會員中心')

@section('content')
			<h1>您的資訊:</h1>
			<table class="table table-bordered">
				<tr>
					<td>您的大名:</td>
					<td>{{ $user->name }}</td>
				</tr>
				<tr>
					<td>電子郵件:</td>
					<td>{{ $user->email }}</td>
				</tr>
				<tr>
					<td>等級:</td>
					@if($user->level == 'root')
						<td><span class="badge badge-success">Root</span></td>
					@elseif($user->level == 'admin')
						<td><span class="badge badge-success">管理員</span></td>
					@elseif($user->level == 'store')
						<td><span class="badge badge-success">商店會員</span></td>
					@elseif($user->level == 'user')
						<td><span class="badge badge-success">一般會員</span></td>
					@else
						<td><span class="badge badge-warning">有問題</span></td>
					@endif
				</tr>
				<tr>
					<td>狀態:</td>
					@if($user->status == 1)
						<td><span class="badge badge-success">已啟用</span></td>
					@endif
				</tr>
				<tr>
					<td>註備:</td>
					<td>請一定要注意交易安全！一旦成功交易等同於不能返還相關費用！</td>
				</tr>
			</table>
@endsection