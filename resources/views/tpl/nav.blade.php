		<ul class="nav nav-tabs justify-content-center">
			<li class="nav-item"><a href="{{ route('index') }}" class="nav-link">首頁</a></li>
			<li class="nav-item"><a href="{{ route('plan') }}" class="nav-link">方案</a></li>
			<li class="nav-item"><a href="{{ route('tos') }}" class="nav-link">服務條款</a></li>
			<li class="nav-item"><a href="{{ route('member') }}" class="nav-link">會員中心</a></li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					錢包功能 <span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a href="{{ route('wallet') }}" class="dropdown-item">我的錢包</a>
					<a href="{{ route('wallet.redeem') }}" class="dropdown-item">儲值</a>
					<a href="{{ route('wallet.log') }}" class="dropdown-item">錢包紀錄</a>
					<div class="dropdown-divider"></div>
					<a href="{{ route('member.showOrderPayFrom') }}" class="dropdown-item">付款紀錄</a>
					<a href="{{ route('member.showOrderPayTo') }}" class="dropdown-item">收款紀錄</a>
				</div>
			</li>
			<li class="nav-item adropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					Api專區 <span class="caret"></span>
				</a>
				<div class="dropdown-menu">
					<a href="{{ route('Api.apply') }}" class="dropdown-item">申請Api</a>
					<a href="{{ route('Api.show') }}" class="dropdown-item">Api資訊</a>
					<div class="dropdown-divider"></div>
					<a href="{{ route('Api.doc')}}" class="dropdown-item">Api文件</a>
				</div>
			</li>			
			@if(Auth::check())
				<li class="nav-item"><a href="{{ route('logout') }}" class="nav-link">登出 【{{ Auth::user()->name }}】</a></li>
			@else
				<li class="nav-item"><a href="{{ route('login') }}" class="nav-link">登入</a></li>
			@endif
		</ul>