	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="{{ asset('bootstrap/4.1.1/css/bootstrap.min.css') }}">
	<!--jQuery-->
	<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
	<!--Popper-->
	<script src="{{ asset('js/popper.min.js') }}"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="{{ asset('bootstrap/4.1.1/js/bootstrap.min.js') }}"></script>
	<!--Custom CSS-->
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<meta name="viewport" content="width=device-width,initial-scale=1">