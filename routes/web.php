<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', 'WebController@index')->name('index');
Route::get('/plan', 'WebController@plan')->name('plan');
Route::get('/tos', 'WebController@tos')->name('tos');
Route::get('/login', 'WebController@login')->name('login');
Route::post('/login', 'WebController@loginc')->name('loginc');
Route::get('/reg', 'WebController@reg')->name('reg');
Route::post('/reg', 'WebController@regc')->name('regc');
Route::get('/logout', 'WebController@logout')->name('logout');

Route::prefix('/member')->middleware('isLogin')->group(function () {
	Route::get('/', 'MemberController@center')->name('member');
	Route::prefix('/api')->middleware('can:view-api')->group(function (){
        Route::get('/', 'ApiController@show')->name('Api.show');
        Route::get('/doc', 'ApiController@doc')->name('Api.doc');
        Route::get('/apply', 'ApiController@apply')->name('Api.apply');
        Route::post('/apply', 'ApiController@applyc')->name('Api.applyc');
    });
	Route::get('/order/pay', 'MemberController@showOrderPayFrom')->name('member.showOrderPayFrom');
	Route::get('/order/get', 'MemberController@showOrderPayTo')->name('member.showOrderPayTo');
});
Route::prefix('/wallet')->middleware('isLogin')->group(function () {
	Route::get('/', 'WalletController@show')->name('wallet');
	Route::get('/redeem', 'WalletController@redeem')->name('wallet.redeem');
	Route::post('/redeem', 'WalletController@redeemc')->name('wallet.redeemc');
	Route::get('/log', 'WalletController@WalletLog')->name('wallet.log');
});

Route::prefix('/admin')->middleware('isLogin:admin')->group(function () {
	Route::get('/', 'AdminController@admin')->name('admin');
	Route::resource('/user', 'Admin\UserController');
	Route::get('/addcode', 'AdminController@AddCode')->name('admin.addCode');
	Route::post('/addcode', 'AdminController@AddCodec')->name('admin.addCodec');
});

Route::prefix('/pay')->group(function () {
	Route::post('/', 'PayController@doPay')->name('pay');
});

Route::prefix('/find')->group(function () {
	Route::post('/', 'PayController@doFind')->name('find');
});

/* Route::group(['prefix' => 'test'], function () {
	Route::get('/', 'TestController@ajax')->name('test.ajax');
	Route::post('/', 'TestController@ajaxc')->name('test.ajaxc');
	Route::get('/show', 'TestController@show')->name('test.show');
}); */

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
