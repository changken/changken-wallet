<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletLog extends Model
{
    /**
     * @var  array $guarded
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function Wallet()
	{
		return $this->belongsTo(Wallet::class);
	}
}
