<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * @var  array $guarded
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];
}
