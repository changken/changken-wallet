<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

		Gate::define('view-admin', function ($user) {
			return $user->level == "admin" || $user->level == "root";
		});

		Gate::define('view-api', function($user){
            return $user->level == "admin" || $user->level == "root" || $user->level == "store";
        });
		
		Gate::define('apply-api', function ($user) {
			return ($user->level == "admin" || $user->level == "root" || $user->level == "store") && $user->Api()->doesntExist();
		});

		Gate::define('view-receive-payment', function ($user){
            return $user->level == "admin" || $user->level == "root" || $user->level == "store";
        });
    }
}
