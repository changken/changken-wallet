<?php
/**
 * 代碼產生器
 * @param int $length
 * @return string
 */
function randtext($length) 
{
	$password_len = $length;	//字串長度
	$password = '';
	$word = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';   //亂數內容
	$len = strlen($word);
	for ($i = 0; $i < $password_len; $i++) {
		$password .= $word[rand() % $len];
	}
	return $password;
}

/**
 * 亂數產生檢查是否存在
 * @param callable|int $generate
 * @param callable $check
 * @return string
 */
function checkDataIfExists($generate, callable $check)
{
    do{
        $data = (is_callable($generate)) ? $generate() : randtext($generate); //產生資料或使用使用者提供的closure
    }while($check($data));//檢查資料是否重複 回傳boolean
	
	return $data;
}

/**
 * 產生錢包代碼
 * @return string
 */
function generateWalletCode()
{
    return sprintf('%s-%s-%s', randtext(3), randtext(3), randtext(3));
}