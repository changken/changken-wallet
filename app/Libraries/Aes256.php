<?php

namespace App\Libraries;

class Aes256
{
	public function encrypt($data,$aes_key,$aes_iv)
	{
		$encrypt = openssl_encrypt($this->addpadding($data),'AES-256-CBC',$aes_key,OPENSSL_RAW_DATA,$aes_iv);
		$encrypt_text = base64_encode($encrypt);
		return $encrypt_text;
	}

	public function decrypt($encrypt_text,$aes_key,$aes_iv)
	{
		$decrypt_text = base64_decode($encrypt_text);
		$data = openssl_decrypt($decrypt_text,'AES-256-CBC',$aes_key,OPENSSL_RAW_DATA,$aes_iv);
		$decrypt_text = $this->strippadding($data);
		return $decrypt_text;
	}

	private function addpadding($data, $blocksize = 32)
	{
		$padding = $blocksize - (strlen($data) % $blocksize);
		$data .= str_repeat(chr($padding), $padding);
		return $data;
	}

	private function strippadding($data)
	{
		$padding = ord($data[strlen($data) - 1]);
		$data = substr($data, 0, -$padding);
		return $data;
	}
}