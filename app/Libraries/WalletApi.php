<?php

namespace App\Libraries;

use App\User;
use Auth;
use App\Wallet;
use App\WalletLog;
use App\Api;
use App\Order;

class WalletApi
{
	//使用changken 錢包支付
	public static function payByChangkenWallet($item,$total,$Note)
	{
		$Wallet = Wallet::where('user_id', Auth::id())->first();

		if($Wallet->value >= $total)
		{
			$Wallet->value -= $total;
			$Wallet->save();

			$walletlog = new WalletLog;
			$walletlog->wallet_id = $Wallet->id;
			$walletlog->item = $item;
			$walletlog->price = -$total;
			$walletlog->value = $Wallet->value;
			$walletlog->note = $Note;
			$walletlog->save();

			return true;
		}
		else
		{
			return false;
		}
	}

	//於收款人的錢包紀錄中產生紀錄
	public static function getMoneyByChangkenWallet($item,$total,$Note,$Api_id)
	{
		$Api = Api::where('Api_id', $Api_id)->first(); //利用Api ID取得api資訊

		if(!$Api) //如果找不到此api 返回false
		{
			return false;
		}
		else
		{
			$user = $Api->user; //利用api關聯取得user

			$wallet = Wallet::where('user_id', $user->id)->first(); //更新錢包餘額
			$wallet->value += $total;
			$wallet->save();

			//產生錢包紀錄
			$walletlog = new WalletLog;
			$walletlog->wallet_id = $wallet->id;
			$walletlog->item = $item."  By Api ID:".$Api_id;
			$walletlog->price = +$total;
			$walletlog->value = $wallet->value;
			$walletlog->note = $Note;
			$walletlog->save();

			return true;
		}
	}

	//產生Api token
	public static function getApiToken($Total,$Api_id,$Date)
	{
		$Api = Api::where('Api_id', $Api_id)->first();

		//產生api token check
		$Api_token_check_arr = [
			'Total' => $Total,
			'Api_id' => $Api_id,
			'Api_key' => $Api->Api_key,
			'Key_pass' => $Api->Key_pass,
			'Date' => $Date
		];

		ksort($Api_token_check_arr);

		$Api_token_check = hash('sha256',http_build_query($Api_token_check_arr));

		return $Api_token_check;
	}

	public static function genOrder($pay_from,$pay_to,$Item,$Total,$Note)
	{
		//產生訂單紀錄
		$order = new Order;
		$order->order_id = checkDataIfExists(25, function ($data){
			return Order::where('order_id', $data)->first();
		});
		$order->pay_from = $pay_from;
		$order->pay_to = $pay_to;
		$order->item = $Item;
		$order->price = $Total;
		// $order->status = '交易成功';
        $order->status = 1;
        $order->note = $Note;
		$order->save();

		return [
			'order_id' => $order->order_id,
			'pay_from' => $order->pay_from,
			'pay_to' => $order->pay_to
		];
	}

	public static function getApiToken_Order($Order_id,$Api_id,$Date)
	{
		$Api = Api::where('Api_id', $Api_id)->first();

		//產生api token check
		$Api_token_check_arr = [
			'Order_id' => $Order_id,
			'Api_id' => $Api_id,
			'Api_key' => $Api->Api_key,
			'Key_pass' => $Api->Key_pass,
			'Date' => $Date
		];

		ksort($Api_token_check_arr);

		$Api_token_check = hash('sha256',http_build_query($Api_token_check_arr));

		return $Api_token_check;
	}
}
