<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'agree' => 'accepted',
            'name' => 'required|string|unique:users,name',
			'email' => 'required|email|unique:users,email',
			'pw' => 'required|string|min:8|same:pw2',
			'pw2' => 'required|string|min:8|same:pw'
        ];
    }
	
	/**
     * 自訂錯誤訊息
     *
     * @return array
     */	
	public function messages()
	{
		return [
			'agree.accepted' => '必須要同意服務條款！',
			'required' => '必須輸入 :attribute ！',
			'string' => ':attribute 必須是字串！',
			'email' => ':attribute 必須是正確的電子郵件格式！',
			'name.unique' => '此帳號已被使用！',
			'email.unique' => '此電子郵件已被使用！',
			'min' => ':attribute 至少應輸入8個字元！',
			'same' => ':attribute 要跟 :other 相同！'
		];
	}
}
