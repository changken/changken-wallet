<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RedeemReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'agree' => 'accepted',
            'key' => 'required|max:11|exists:wallet_codes,code'
        ];
    }
	
    /**
     * 自訂錯誤訊息
     *
     * @return array
     */	
	public function messages()
	{
		return [
			'agree.accepted' => '必須要同意服務條款！',
			'key.required' => '必須輸入錢包代碼！',
			'key.exists' => '錢包代碼無效！',
			'key.max' => '錢包代碼長度不得超過11個字！'
		];
	}
}
