<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Api;
use App\Wallet;
use App\Order;
use App\Libraries\WalletApi;

class PayController extends Controller
{
	public function doPay(Request $request)
	{
		$Item = $request->input('Item');
		$Total = $request->input('Total');
		$Note = $request->input('Note');
		$Return_url = $request->input('Return_url');
		$Api_id = $request->input('Api_id');
		$Date = $request->input('Date');
		$Api_token = $request->input('Api_token');
		
		if($Item == null)
		{
			$response = json_encode([
				'code' => 10,
				'message' => 'Item 欄位不能為空！'
			]);			
		}
		else if($Total == null)
		{
			$response = json_encode([
				'code' => 11,
				'message' => 'Total 欄位不能為空！'
			]);	
		}
		else if($Api_id == null)
		{
			$response = json_encode([
				'code' => 12,
				'message' => 'Api_id 欄位不能為空！'
			]);				
		}
		else if($Date == null)
		{
			$response = json_encode([
				'code' => 13,
				'message' => 'Date 欄位不能為空！'
			]);				
		}
		else if($Api_token == null)
		{
			$response = json_encode([
				'code' => 14,
				'message' => 'Api_token 欄位不能為空！'
			]);				
		}
		else
		{
			$Api = Api::where('Api_id', $Api_id)->first();
		
			if(!$Api || $Api->status == 'close')
			{
				$response = json_encode([
					'code' => 100,
					'message' => '錯誤！您沒有使用此api的權限！'
				]);		
			}
			else if($Api_token != WalletApi::getApiToken($Total,$Api_id,$Date))
			{
				$response = json_encode([
					'code' => 111,
					'message' => '錯誤！api token驗證失敗！'
				]);		
			}
			else if(!Auth::check())
			{
				$response = json_encode([
					'code' => 101,
					'message' => '錯誤！您尚未登入changken 錢包！'
				]);
			}
			else if(!WalletApi::payByChangkenWallet($Item,$Total,$Note))
			{
				$response = json_encode([
					'code' => 102,
					'message' => '錯誤！您的changken 錢包餘額不足！'
				]);
			}
			else if(!WalletApi::getMoneyByChangkenWallet($Item,$Total,$Note,$Api_id))
			{
				$response = json_encode([
					'code' => 110,
					'message' => '錯誤！changken 錢包內部錯誤！請立即聯繫管理員！'
				]);
			}
			else
			{
				$pay_from = User::findOrFail(Auth::id())->wallet->address;
				$pay_to = $Api->user->wallet->address;
				$order = WalletApi::genOrder($pay_from,$pay_to,$Item,$Total,$Note);
				$response = json_encode([
					'code' => 103,
					'message' => '成功！您已支付此款項！',
					'data' => [
						'order_id' => $order['order_id'],
						'pay_from' => $order['pay_from'],
						'pay_to' => $order['pay_to']
					]
				]);
			}
		}
		
			if($Return_url == null)
			{
				return view('pay.dopay2',[
					'Response' => json_decode($response)
				]);
			}
			else
			{
				return view('pay.dopay',[
					'Return_url' => $Return_url,
					'Response' => $response
				]);
			}
	}
	
	public function doFind(Request $request)
	{
		$Order_id = $request->input('Order_id');
		$Return_url = $request->input('Return_url');
		$Api_id = $request->input('Api_id');
		$Date = $request->input('Date');
		$Api_token = $request->input('Api_token');
		
		if($Order_id == null)
		{
			$response = json_encode([
				'code' => 200,
				'message' => 'Order_id 欄位不能為空！'
			]);			
		}
		else if($Api_id == null)
		{
			$response = json_encode([
				'code' => 201,
				'message' => 'Api_id 欄位不能為空！'
			]);				
		}
		else if($Date == null)
		{
			$response = json_encode([
				'code' => 202,
				'message' => 'Date 欄位不能為空！'
			]);				
		}
		else if($Api_token == null)
		{
			$response = json_encode([
				'code' => 203,
				'message' => 'Api_token 欄位不能為空！'
			]);				
		}
		else
		{
			$Api = Api::where('Api_id', $Api_id)->first();
		
			if(!$Api || $Api->status == 'close')
			{
				$response = json_encode([
					'code' => 204,
					'message' => '錯誤！您沒有使用此api的權限！'
				]);		
			}
			else if($Api_token != WalletApi::getApiToken_Order($Order_id,$Api_id,$Date))
			{
				$response = json_encode([
					'code' => 205,
					'message' => '錯誤！api token驗證失敗！'
				]);		
			}
			else
			{
				$Order = Order::where('order_id', $Order_id)->first();
				$response = json_encode([
					'code' => 206,
					'message' => '成功！查到的交易資訊如下:',
					'data' => [
						'order_id' => $Order->order_id,
						'pay_from' => $Order->pay_from,
						'pay_to' => $Order->pay_to,
						'item' => $Order->item,
						'price' => $Order->price,
						'status' => $Order->status,
						'note' => $Order->note,
						'tdate' => (string) $Order->created_at
					]
				]);
			}
		}
		
			if($Return_url == null)
			{
				return view('pay.dofind2',[
					'Response' => json_decode($response)
				]);
			}
			else
			{
				return view('pay.dofind',[
					'Return_url' => $Return_url,
					'Response' => $response
				]);
			}	
	}
}
