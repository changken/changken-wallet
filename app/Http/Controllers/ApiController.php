<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ApiRepository;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    protected $apiRepository;

    public function __construct(ApiRepository $apiRepository)
    {
        $this->apiRepository = $apiRepository;
    }

    //申請Api金鑰
	public function apply()
	{
		return view('api.apply');
	}
	
	public function applyc()
	{
		//新增Api
        $this->apiRepository->create([
            'user_id' => Auth::id(),
            'Api_id' => checkDataIfExists(10, function($data){
                return $this->apiRepository->isApiIdExist($data);
            }),
            'Api_key' => checkDataIfExists(15, function($data){
                return $this->apiRepository->isApiKeyExist($data);
            }),
            'Key_pass' => randtext(30),
            'Status' => 'open'
        ]);
		
		return '<div class="alert alert-success"><h2 class="text-center">申請成功！</h2></div>';
	}
	
	//顯示Api金鑰
	public function show()
	{
	    if(Auth::user()->can('apply-api')){
	        return redirect()->route('Api.apply');
        }

		return view('api.show',[
			'Api' => Auth::user()->Api
		]);
	}
	
	//介紹Api
	public function doc()
	{
		return view('api.doc');
	}   
}
