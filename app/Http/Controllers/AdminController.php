<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WalletCodeRepository;


class AdminController extends Controller
{
    protected $walletCodeRepository;

    public function __construct(WalletCodeRepository $walletCodeRepository)
    {
        $this->walletCodeRepository = $walletCodeRepository;
    }

    public function admin()
	{
		return view('admin.index');
	}
	
    //新增錢包代碼
	public function AddCode()
	{	
		return view('admin.addcode');
	}
	
	public function AddCodec(Request $request)
	{
		$walletCode = $this->walletCodeRepository->create([
		    'code' => checkDataIfExists(function(){
                return generateWalletCode();
            }, function ($data){
		        return $this->walletCodeRepository->isCodeExist($data);
            }),
            'value' => $request->input('value')
        ]);
		
		return view('admin.addcodec',[
			'key' => $walletCode->code,
			'value' => $request->input('value')
		]);
	}
}
