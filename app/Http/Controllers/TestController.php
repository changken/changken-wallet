<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function ajax()
	{
		return view('test.ajax');
	}
	
	public function ajaxc(Request $request)
	{
		if($request->input('key') == "K1E-J48-99K")
		{
			$state = 'success';
			$msg = '儲值成功！錢包代碼不可重複使用，若對於此筆交易有異議者可向客服查詢！';
		}
		else
		{
			$state = 'fail';
			$msg = '儲值錯誤！此錢包代碼已經被使用過了！';
		}
		
		if($state == "success")
		{
			return response()->json(["response" => "<div class=\"alert alert-success\">$msg</div>"]);
		}
		else
		{
			return response()->json(["response" => "<div class=\"alert alert-danger\">$msg</div>"]);
		}
	}
	public function show()
	{
		return view('test.show');
	}
}
