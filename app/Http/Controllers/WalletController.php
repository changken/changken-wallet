<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\WalletLogRepository;
use App\Services\WalletService;
//use App\Http\Requests\RedeemReq;

class WalletController extends Controller
{
    protected $walletLogRepository;
    protected $walletService;

    public function __construct(WalletLogRepository $walletLogRepository, WalletService $walletService)
    {
        $this->walletLogRepository = $walletLogRepository;
        $this->walletService = $walletService;
    }

    //顯示錢包
	public function show()
	{
		return view('wallet.show', [
		    'user' => Auth::user()
        ]);
	}
	
	//兌換錢包代碼
	public function redeem()
	{
		return view('wallet.redeem');
	}

    public function redeemc(Request $request)
	{
		$response = $this->walletService->redeemWalletCode($request->all());
		
		if($response['status'])
		{
			return "<div class=\"alert alert-success\">{$response['msg']}</div>";
		}
		else
		{
			return "<div class=\"alert alert-danger\">{$response['msg']}</div>";
		}
	}
	
	//顯示錢包紀錄
	public function WalletLog()
	{
		return view('wallet.Log',[
			'WalletLogs' => $this->walletLogRepository->getLogByWalletID(Auth::user()->Wallet->id)
		]);
	}
}
