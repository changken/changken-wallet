<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegReq;
use App\Repositories\UserRepository;
use App\Repositories\WalletRepository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class WebController extends Controller
{
    protected $userRepository;
    protected $walletRepository;

    public function __construct(UserRepository $userRepository, WalletRepository $walletRepository)
    {
        $this->userRepository = $userRepository;
        $this->walletRepository = $walletRepository;
    }

    //首頁
    public function index()
	{
		return view('web.index');
	}
	
	//方案
	public function plan()
	{
		return view('web.plan');
	}
	
	//服務條款
	public function tos()
	{
		return view('web.tos');
	}
	
	//登入
	public function login()
	{
		return view('web.login');
	}
	
	public function loginc(Request $request)
	{
		$name = $request->input('name');
		$pw = $request->input('pw');
		$remember = $request->input('remember');
		
		if(Auth::attempt(['name' => $name, 'password' => $pw, 'status' => 1],$remember))
		{
			return redirect()->route('member');
		}
		else
		{
			return redirect()->route('login');
		}
	}
	
	//登出
	public function logout()
	{
		Auth::logout();
		return redirect()->route('login');
	}
	
	//註冊
	public function reg()
	{
		return view('web.reg');
	}

	public function regc(RegReq $request)
	{
	    $user = $this->userRepository->create([
	        'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('pw')),
            'level' => 'user',
            'status' => 1
        ]);

	    //順便新增錢包
	    $this->walletRepository->create([
	        'user_id' => $user->id,
            'address' => checkDataIfExists(20, function ($data){
                return $this->walletRepository->isAddressExist($data);
            }),
	        'value' => 0
        ]);
			
		return redirect()->route('login');
	}
}
