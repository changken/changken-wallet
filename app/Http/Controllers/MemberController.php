<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\OrderRepository;

class MemberController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    //會員中心
	public function center()
	{
		return view('member.center', [
			'user' => Auth::user()
		]);
	}

	//顯示訂單紀錄(支付方)
	public function showOrderPayFrom()
	{
		return view('member.showOrderPayFrom',[
			'Orders' => $this->orderRepository->getAllPayFrom(Auth::user()->Wallet->address)
		]);
	}
	
	//顯示訂單紀錄(收款方)
	public function showOrderPayTo()
	{
		return view('member.showOrderPayTo',[
			'Orders' => $this->orderRepository->getAllPayTo(Auth::user()->Wallet->address)
		]);
	}
}
