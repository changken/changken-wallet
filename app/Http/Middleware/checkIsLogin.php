<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class checkIsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param string $level
     * @return mixed
     */
    public function handle($request, Closure $next, $level = null)
    {
		if(!Auth::check())
		{
			return redirect()->route('login');
		}
		else if($level == "admin" && Gate::denies('view-admin'))
		{
			return redirect()->route('member');
		}
		
        return $next($request);
    }
}
