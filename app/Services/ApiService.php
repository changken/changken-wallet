<?php

namespace App\Services;

use App\Repositories\ApiRepository;

class ApiService
{
    protected $apiRepository;

    public function __construct(ApiRepository $apiRepository)
    {
        $this->apiRepository = $apiRepository;
    }
}