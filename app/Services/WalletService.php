<?php

namespace App\Services;

use App\Repositories\WalletRepository;
use App\Repositories\WalletCodeRepository;
use App\Repositories\WalletLogRepository;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class WalletService
{
    protected $walletRepository;
    protected $walletCodeRepository;
    protected $walletLogRepository;

    public function __construct(WalletRepository $walletRepository,
                                WalletCodeRepository $walletCodeRepository,
                                WalletLogRepository $walletLogRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->walletCodeRepository = $walletCodeRepository;
        $this->walletLogRepository = $walletLogRepository;
    }

    /**
        * @param array $data
        * @return array
        */
    public function redeemWalletCode(array $data)
    {
        $walletCode = $this->walletCodeRepository->getByCode($data['key']);

        $User = Auth::user();

        if(!isset($walletCode))
        {
            $response = [
                'status' => false,
                'msg' => '儲值錯誤！此錢包代碼無效！'
            ];
        }
        elseif(isset($walletCode->used_at))
        {
            $response = [
                'status' => false,
                'msg' => '儲值錯誤！此錢包代碼已經被使用過了！'
            ];
        }
        else
        {
            $this->walletCodeRepository->update($walletCode->id, [
                'wallet_id' => $User->Wallet->id,
                'used_at' => Carbon::now()
            ]);

            $this->walletRepository->update($User->Wallet->id, [
                'value' => $User->Wallet->value + $walletCode->value
            ]);

            $this->walletLogRepository->create([
                'wallet_id' => $User->Wallet->id,
                'item' => '錢包儲值 NT$' . $walletCode->value,
                'price' => $walletCode->value,
                'value' => $User->Wallet->value + $walletCode->value,
                'note' => '錢包代碼:' . $walletCode->code
            ]);

            $response = [
                'status' => true,
                'msg' => '儲值成功！錢包代碼不可重複使用，若對於此筆交易有異議者可向客服查詢！'
            ];
        }

        return $response;
    }

}