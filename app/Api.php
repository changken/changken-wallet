<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    /**
     * @var  array $guarded
     */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function User()
	{
		return $this->belongsTo(User::class);
	}
}
