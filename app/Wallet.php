<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    /**
        * @var  array $guarded
        */
    protected $guarded = [
        'id',
        'created_at',
        'updated_at'
    ];

    public function User()
	{
		return $this->belongsTo(User::class);
	}
	
	public function WalletLog()
	{
		return $this->hasMany(WalletLog::class);
	}
	
    public function WalletCode()
	{
		return $this->hasMany(WalletCode::class);
	}
}
