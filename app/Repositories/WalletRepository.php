<?php

namespace App\Repositories;

use App\Wallet;

class WalletRepository extends Repository
{
    public function __construct(Wallet $wallet)
    {
        parent::__construct($wallet);
    }

    /**
        * check wallet address if it exists.
        * @param string $address
        * @return bool
        */
    public function isAddressExist($address)
    {
        return $this->model->where('address', $address)->exists();
    }
}