<?php

namespace App\Repositories;

use App\Api;

class ApiRepository extends Repository
{
    public function __construct(Api $api)
    {
        parent::__construct($api);
    }

    /**
        * check api id if it exists.
        * @param string $apiId
        * @return bool
        */
    public function isApiIdExist($apiId)
    {
        return $this->model->where('Api_id', $apiId)->exists();
    }

    /**
        * check api key if it exists.
        * @param string $apiKey
        * @return bool
        */
    public function isApiKeyExist($apiKey)
    {
        return $this->model->where('Api_key', $apiKey)->exists();
    }
}