<?php

namespace App\Repositories;

use App\Order;

class OrderRepository extends Repository
{
    public function __construct(Order $order)
    {
        parent::__construct($order);
    }

    /**
        * 付款紀錄
        * @param string $address
        * @return \Illuminate\Database\Eloquent\Collection
        */
    public function getAllPayFrom($address)
    {
        return $this->model->where('pay_from', $address)
            ->orderBy('id', 'desc')
            ->paginate(5);
    }

    /**
        * 收款紀錄
        * @param string $address
        * @return \Illuminate\Database\Eloquent\Collection
        */
    public function getAllPayTo($address)
    {
        return $this->model->where('pay_to', $address)
            ->orderBy('id', 'desc')
            ->paginate(5);
    }
}