<?php

namespace App\Repositories;

use App\WalletLog;

class WalletLogRepository extends Repository
{
    public function __construct(WalletLog $walletLog)
    {
        parent::__construct($walletLog);
    }

    /**
        * get wallet log using wallet id
        * @param int $walletId
        * @return \Illuminate\Database\Eloquent\Collection
        */
    public function getLogByWalletID($walletId)
    {
        return $this->model->where('wallet_id', $walletId)->orderBy('id', 'desc')->paginate(5);
    }
}