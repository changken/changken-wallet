<?php

namespace App\Repositories;

use App\WalletCode;

class WalletCodeRepository extends Repository
{
    public function __construct(WalletCode $walletCode)
    {
        parent::__construct($walletCode);
    }

    /**
        * get wallet code using code
        * @param string $code
        *  @return
        */
    public function getByCode($code)
    {
        return $this->model->where('code', $code)->first();
    }

    /**
        *  check wallet code if it exists.
        * @param string $code
        * @return bool
        */
    public function isCodeExist($code)
    {
        return $this->model->where('code', $code)->exists();
    }
}